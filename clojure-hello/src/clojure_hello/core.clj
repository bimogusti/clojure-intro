(ns clojure-hello.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn doubleIt 
  "This is function  to double the input"
  [x] (* x 2))

(defn greet
  "Say hello to me"
  [name] (str "hello " name)
  )

(defn nullify [x](- x x))

(comment  
  
  (doubleIt 2)
  
  (greet "bimo")
  
  (nullify 8)

  )
